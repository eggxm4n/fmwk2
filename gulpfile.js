var gulp              = require('gulp');
var browserSync       = require('browser-sync');
var sass              = require('gulp-sass');
var autoprefixer      = require('gulp-autoprefixer');
var mq                = require('gulp-combine-mq');
var sourcemaps        = require('gulp-sourcemaps');
var cache             = require('gulp-cache');
var imagemin          = require('gulp-imagemin');

var srcDir            = './src';

var sassDir           = srcDir + '/scss';
var jsDir             = srcDir + '/js';
var imgDir            = srcDir + '/img';

var distDir           = './dist';




gulp.task('sass', function() {
  gulp.src(sassDir + '/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(sourcemaps.write()) // Sourcemaps initialisé pour le dev
    .pipe(autoprefixer({
      browsers: ['last 2 versions'], // Check documentation @ https://github.com/ai/browserslist#queries
    }))
    .pipe(gulp.dest(distDir + '/css/'))
    .pipe(browserSync.stream());
})

gulp.task('fonts', function() {

  return gulp.src(srcDir + '/fonts/**/*')
    .pipe(gulp.dest(distDir + '/fonts'));

});

gulp.task('html', function(){
  return gulp.src(srcDir + '/*.html')
  .pipe(gulp.dest(distDir));
});


gulp.task('minify-img', function() {
  return gulp.src(srcDir + '/img/**/*.+(png|jpg|jpeg|gif|svg)')
    .pipe(cache(imagemin({
      progressive: true, // jpg
      optimizationLevel: 4 // png
    })))
    .pipe(gulp.dest(distDir + '/img'));
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: distDir,
    },
    reloadOnRestart: true,
  });
});

gulp.task('default', ['sass'], function() {

});

gulp.task('watch', ['browser-sync'], function(){
  gulp.watch(sassDir + '/**/*.scss', ['sass']);
  gulp.watch(srcDir + '/*.html',  ['html', browserSync.reload]);
  gulp.watch(jsDir + '/**/*.js', browserSync.reload);
});
